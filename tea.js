/* tea [js gui & tools lib] v2023.01 Jirka Justra */
(()=>{
	function C(k,v,u){// config
		!C._d&&(C._d={
			_s:'width:100%',
			E_s:'',
			L_s:'padding:5px;color:#fff;font-size:16px',
			B_s:'text-align:center;',
			T_s:'height:300px;min-width:100%;max-width:100%;',
			G_s:'z-index:9999;padding:5px;margin:2px;position:fixed;top:0px;left:0px;width:30%;height:auto;background-color:rgba(0,0,0,.5);color:#000;font-family:sans-serif;',
			log_lvl:4,
			log_fc:console.log,
			click_fc:function(f,d,p,e){f&&f(d,p,e)}
		});
		if(k===u)return C._d;
		(v!==u)&&(C._d[k]=v);
		return C._d[k]
	}
	function S(e,s,a,i,_){// style
		s=s||'';
		a=a||s.split(';');
		i=i||0;
		if(!a[i])return e;
		_=a[i].split(':');
		e.style[_[0]]=_[1];
		return S(e,s,a,i+1)
	}
	function A(e,p){// append
		p=p||C('parent')||document.body;
		p.appendChild(e);
		return e
	}
	function O(e,d, p,k,ka,i){// set some obj props
		for (k in d) {
			ka=k.split('.')
			p=e
			for (i=0;i<ka.length-1;i++)
				p=p[ka[i]]
			p[ka[i]]=d[k]
		}
		return e
	}
	function E(t,s,p,o){// element
		var e=document.createElement(t);
		if(p){
			if((typeof(p))[0]!='o')p=0;
			e=A(e,p)
		}
		o&&O(e,o)
		return S(e,s||C('_s'))
	}
	function G(s,e,u){// gui
		s===u&&(s=G._i);
		s<0&&(s=G._l.length+s);
		if(s>0||s===0){
			G._i=s;
			//~ e!==u&&e<0()
			return (e==u)&&G._l[s]||G._l[s].childNodes[e>=0?e:G._l[s].childNodes.length+e]
		}
		s=C('G_s')+(s||'');
		G._l=G._l||[];
		G._l.push(S(A(e||E('div')),s));
		G._i=-1;
		return G._l.length-1
	}
	function D(e,d,a,i){// gui data
		if(e>=0||e<0||!e)e=G(e).firstChild;
		a=a||[];
		i=i||0;
		if(['INPUT','TEXTAREA'].indexOf(e.tagName)+1){
			a.push(e.value);
			if(d&&i<d.length)e.value=d[i++]
		}
		e=e.nextSibling;
		if(e)return D(e,d,a,i);
		return a
	}
	function B(s,f,e){e=e||A(S(E('button'),C('B_s')),G());s&&(e.innerText=s);e.onclick=(e)=>{e.stopPropagation();e=e.originalTarget||e.target;C('click_fc')(f,D(e.parentNode.firstChild),e.parentNode,e)};return e}
	function T(s,e){e=e||A(E('textarea'),G());s=C('T_s')+(s||'');return S(e,s)}
	function Tea(s){
		Tea.s=s;
		Tea.i=0;
		Tea.g=[];
		Tea._a=[];
		for(var i=1;i<arguments.length;i++)Tea._a.push(arguments[i]);
		return Tea
	}
	Tea.A=function (i,c) {i=i||Tea.i;c=Tea.s.charAt(i);if(!c||c=='|'||c.toLowerCase()!=c){ var s=Tea.s.substr(Tea.i,i-Tea.i);Tea.i=i+(c=='|');return s };return Tea.A(i+1)};
	Tea.XG=function () {Tea.g.push(G(Tea.A()))};
	Tea.XB=function () {B(Tea.A()||Tea._a.shift(),Tea.A()||Tea._a.shift())};
	Tea.XT=function () {T(Tea.A())};
	Tea.XE=function () {A(S(E('input'),C('E_s')),G())};
	Tea.XL=function () {A(S(E('span') ,C('L_s')),G()).innerText=(Tea.A()||Tea._a.shift())};
	Tea.X=function (c,f) {c=c||Tea.s.charAt(Tea.i++);if(!c)return Tea.g;f=f||Tea['X'+c];f&&f();return Tea.X()};
	Tea.S=S;
	Tea._=A;
	Tea.E=E;
	Tea.G=G;
	Tea.D=D;
	Tea.C=C;
	window['T']=Tea;
})();

function Q(s,p){return (p||document).querySelectorAll(s)}
function L(a,f,r, u,_,k)
{
	if (typeof a == 'number') return L(new Array(a).fill(0),f,r, u);
	r = r||[];
	if (a) {
		if (a.constructor != Object) {
			for (k = 0; k < a.length; k ++) {
				_ = f ? f(a[k],k,r,u) : a[k];
				if (_ !== u) r.push(_);
			}
		} else {
			for (k in a) {
				if(!isNaN(k)) k=parseInt(k)
				_ = f ? f(a[k],k,r,u) : a[k];
				if (_ !== u) r.push(_);
			}
		}
	}
	return r;
}

function A(b,a,i)
{
	if (b !== null) {
		a = L(arguments);
		return A(null, a,0)
	}
	
	var res, j;
	res = new Array(a[i])
	
	if (i < a.length - 1) {
		for (j=0; j<a[i]; j++)
			res[j] = A(null, a,i+1)
	}
	
	return res;
}
function A0(b,a,i)
{
	if (b !== null) {
		a = L(arguments);
		return A0(null, a,0)
	}
	
	var res, j;
	res = new Array(a[i])
	
	if (i < a.length - 1) {
		for (j=0; j<a[i]; j++)
			res[j] = A0(null, a,i+1)
	} else
		res.fill(0);
	
	return res;
}

function LA(n0,n1, f, r)
{

	if (typeof(n1) == 'function' || typeof(n1) == 'undefined') {
		// 1D array

		var i;
		r = f || [];
		f = n1;

		for (i=0; i<n0; i++) {
			_r = f ? f(i,i, r) : i;
			if (_r !== undefined) r.push(_r)
		}
		
		return r;
	}


	// 2D array

	if (r !== undefined) {
		// Manual result management

		L(A0(n0), function (_,x) {
			L(A0(n1), function (_,y) {
				return f && f(x,y, r);
			}, r);
		});

	} else {
		// Auto result management

		r = L(A0(n0), function (_,x) {
			return L(A0(n1), function (_,y) {
				return f && f(x,y);
			});
		});

	}

	return r;
}

var LQ=(s,f,o)=>{return L(Array.from(Q(s)),f,o)},TS=T.S,TA=T._,TE=T.E,TG=T.G,TD=T.D,TC=T.C,TX=function(){return T.apply(0,arguments).X()};/* shortcuts */

// loglib
function __log_proc(s,a,sep,i,r) {
	i=i||0;
	r=r||[a.shift()];
	var rI,bA,bR;
	rI=r.length-1;
	bR=(typeof r[rI])[0]=='s';
	bA=(typeof a[i])[0]=='s';
	if(s&&bR&&!bA){
		r[rI]='%c'+r[rI];
		r.push(s);
		s=''
	}
	if(i==a.length)return r;
	if(bR&&bA)
		r[rI]+=sep+a[i];
	else
		r.push(a[i]);
	return __log_proc(s,a,sep,i+1,r)
}
function E(){if(TC('log_lvl')>0){var a=L(arguments);a.unshift('#err',new Date().toISOString(),(new Error).stack.split('\n')[1]);TC('log_fc').apply(0,__log_proc('color:#f55',a,' : '))}return 0}
function W(){if(TC('log_lvl')>1){var a=L(arguments);a.unshift('#wrn',new Date().toISOString(),(new Error).stack.split('\n')[1]);TC('log_fc').apply(0,__log_proc('color:#f5f',a,' : '))}return 0}
function I(){if(TC('log_lvl')>2){var a=L(arguments);a.unshift('#inf',new Date().toISOString(),(new Error).stack.split('\n')[1]);TC('log_fc').apply(0,__log_proc('color:#55f',a,' : '))}return 0}
function D(){if(TC('log_lvl')>3){var a=L(arguments);a.unshift('#dbg',new Date().toISOString(),(new Error).stack.split('\n')[1]);TC('log_fc').apply(0,__log_proc('color:#444',a,' : '))}return 0}
function P(){var a=L(arguments);a.unshift('> ');TC('log_fc').apply(0,__log_proc('color:none',a,' '));return 0}

// popUp library
(()=>{
	var _popup_list=[]
	function open(_e, z,e){
		z = z||(999998 + _popup_list.length * 2);
		e = e||TA(TE('div','width:100%;height:100%;position:fixed;top:0px;left:0px;background:#0008;z-index:' + z))
		e.onclick=close
		if ((typeof _e)[0]=='n') _e=TG(_e);// number to element conv
		_popup_list.push([_e,e])
		e.style.display=''
		_e.style.display=''
		_e.style.zIndex=(z + 1).toString()
	}
	function count(){ return _popup_list.length }
	function close(ev){
		var a
		if (_popup_list.length==0) return
		a=_popup_list.pop()
		a[1].remove()
		a[0].style.display='none'
		if (ev)
			ev.stopPropagation();
		document.activeElement.blur()
	}
	T.popUp=open
	T.popDown=close
	T.popCount=count
})();
