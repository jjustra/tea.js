Migrated to https://gitlab.com/jjustra/tea.js

# tea.js

js gui & tools lib - made specialy for injecting gui into page

0] TL;DR:

	Open web console, copy content of tea.js file there and run :
	
		TX('GTBok',(d,p,e)=>{ P(d) })
	
	Snippet will render Gui with Textbox and Button; write something in textbox and
	click button, then data will be printed into web console.
	
	For details please refer to section 2 and 3


1] Base general functions and their aliases :

	T.S|TS (e,s) - style given element
		args :
			@e - required - element to style
			@s - required - css style string
		returns : @e
	T._|TA (e,p) - appendChild to parent or document.body
		args :
			@e - required - element
			@p - optional - parent element
		returns : @e
	T.E|TE (t)   - creates element
		args :
			@t - required - tag name
		returns : @e
	T.G|TG (s,e) - do stuff with gui in two alternate ways :
		note : gui messed with, becomes default gui (see T.D and T.X)
		args :
			@s - required - css style string
			@e - optional - element to use as gui container; if ommited new DIV is created
		returns : ID of created gui
		args :
			@s - required - ID of gui to point to
			@e - optional - ID of field (its numerical index in gui)
		returns : field's DOM element
		returns : gui's container DOM element - if @e is ommited
	T.D|TD (e,d) - data get/set
		args :
			@e - optional - gui ID if ommited default is used (see T.G)
			@d - optional - data to fill into gui
		returns : old data - if @d is present
		returns : data
	T.C|TC (k,v) - config interface
		args :
			@k - optional - config key to set or get (if v is ommited)
			@v - optional - value to set
		returns : value of given key
		returns : whole config object - if @k is ommited
		valid keys :
			_s       - default style for newly created element
			G_s      - gui container's style
			B_s      - button's style
			T_s      - textbox's style
			E_s      - entry's style
			L_s      - label's style
			click_fc - function for processing button's callback (see function B in section 3 for details)
			log_lvl  - log verbosity (0-4 > nothing,errors,warnings,infos,debugs)


2] Tea GUI creation functions :

	T (s,...)  - loads gui creation script and arguments for it (see next section)
		args :
			@s - required - gui script (see section 3)
			... - arguments for script (button's callbacks/labels or label's texts)
		returns : T
	T.X ()     - executes loaded script - if G function (see next section) is left out from script,
			default gui (see note on T.G) is appended to
		args :
		returns : list of created gui's IDs
	TX (s,...) - joins T and T.X functionnality into one call
		args :
			@s - required - gui script (see section 3)
			... - 
		returns : list of created gui's IDs
	T.A ()     - reads argument from current script
		args :
		returns : argument text


3] Gui creation string functions :

	It is used as first arg in T and TX functions.
	Required arguments can be passed in G's script or in X's arguments. Optional only by G's script.
	Arguments in script are lower case only (+any special chars, except |) strings separated by |.

	G - create new Gui - this is usually first char of script - you can create as many guis as you wish in one script
		@css      - optional - css styling of container gui's element (defaults in code at ln 7)
	B - add Button - clickable element, both arguments can be supplied in script or via X's arguments
		@label    - required - button label
		@callback - required - is evaluated by function stored in 'click_fc' config key
			default is function accepting three args:
				d - gui_data           - array containg value of every element in gui
				p - gui_parent_element - DOM element of gui's container
				e - button_element     - DOM element of clicked button
	T - add Text   - multiline text field
		@css      - optional - css styling of text element - usually for modifing default height (300px)
	E - add Entry  - one line text entry
	L - add Label  - suitable for labeling other elements
		@text     - required - label text

	examples :
		Creates simple gui with textbox and ok button
		TX('GTBok',()=>{ alert('button clicked') })
		
		This call creates button, opening context gui on click
		TX(
			// creates two guis; first with button only, second hidden
			'Gwidth:73pxB]-[ Gdisplay:none;left:80pxLentryELtextTBok',
			
			// callback function for main button
			(d,p,e)=>{
				// switch second's gui visibility
				p.nextSibling.style.display=(p.nextSibling.style.display=='block')?'none':'block';
			},
			
			// callback function for ok button
			(d,p,e)=>{
				// print gui data
				P(d);
				// hide this second gui
				p.style.display='none';
			}
		)


4] Utility functions :

	P - print - uses console.log function
	Q - query select - alias for document.querySelectorAll
		args :
			@s - selector
			@p - optional - parent of query, if ommited, document is used as default
	L - loop - maps array items to function, can be used also as filter function
		args :
			@a - required - array to process
			@f - required - function - accepts :
					o - item itself
					i - index of item
					r - @o (see next arg) or result array
				returns : what should be added to result array
				note : if undefined is returned, it is not added to result - it is filtered out
			@o - optional - object to pass to function as 3rd arg; if ommited, result array is used
		returns: result array
		returns: @o - if given in arguments
	A - array - creates any-dimensional array (depends on number of given arguments)
		args :
			@dim0 - array dimension
			...
			@dimx
	A0 - array - creates any-dimensional array (depends on number of given arguments) - values as set to zero
		args :
			@dim0 - array dimension
			...
			@dimx

	examples :
		L(['a','b','c'],(s,i,r)=>{ P('item index is',i, 'and item value',s, 'result array so far is',r); return [i,s] })
		A(5,5,5)

5] Shortcut Utility functions :

	LQ - loops query selection
	LA - loops (1 or 2-dimensional) array

	examples :
		LQ('img', (e,i)=>{ P('image element #'+i+' source', e.src) })
		LA(2,2, (x,y)=>{ P('x is',x,'y is',y) });

6] Logging functions :
	
	Just prints all arguments with colored prefix and caller data - uses console.log function

	E(...) - log error
	W(...) - log warning
	I(...) - log info
	D(...) - log debug
	
	examples :
		E('this is err msg with array',[1,2,3])

